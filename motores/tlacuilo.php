<?php
/**********************************************************************************************
*    Hanumat. PHP framework for fast and secure web application development
*
*    This file is part of Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*
*    Hanumat is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
************************ Español ********************************
*
*    Hanumat. Entorno de trabajo PHP para desarrollo rápido y seguro de aplicaciones
*
*    Este archivo es parte de Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat es software libre: usted puede redistribuirlo y/o modificarlo
*    bajo los términos de la Licencia Pública General GNU Affero tal y como
*    ha sido publicada por la Free Software Foundation, tanto la versión 3
*    de la Licencia o cualquier otra posterior.
*
*    Hanumat es distribuido en la esperanza de que llegue a ser útil,
*    pero SIN NINGUNA GARANTÍA; incluso sin implicar la garantía de
*    MERCHANTABILITY o FITNESS FOR A PARTICULAR PURPOSE.  Por favor refiérase a la
*    Licencia Pública General GNU Affero para más detalles.
*
*    Usted tiene que haber recibido una copia de la Licencia Pública General GNU Affero
*    con éste programa. De no ser así, vea <https://www.gnu.org/licenses/>.
*
**************************************************************************************/
/**
 * El que se encarga de las impresiones. Su salida suele ser un PDF.
 * 
 */
require_once('interno/defs.php');
require_once('interno/funciones.php');
if ((isset($_SERVER["HTTP_ORIGIN"]) && $_SERVER["HTTP_ORIGIN"] == DOMINIO) || strpos($_SERVER['HTTP_USER_AGENT'], "Firefox") !== FALSE) {
	session_start();
	//Aquí validamos que exista la variable de usuario, como override se puede establecer $hayUsuario = TRUE y luego poner valores arbitrarios.
	$hayUsuario = isset($_SESSION['Usuario']) && isset($_SESSION['Usuario']['publica']);
	if ($hayUsuario) {
		if (isset($_POST['t'])) {
			//recordad, niños: r es la página de retorno, pero nos sirve también para saber que es una consulta y un montón de cosas más.
			$prtParams = array();
			if (strpos($_POST['t'], "||") !== FALSE) {
				//Modo con ID claro
				error_log("Entrando al modo con ID en claro...");
				$pet = explode("||", $_POST['t'], 2);
				$peticion = esclarece($pet[0]);
				$arrPet = explode("|-|", $peticion);
				if (count($arrPet) == 2) {
					$prtParams = $arrPet;
					$prtParams[2] = $pet[1];
				} else {
					$prtParams = NULL;
				}
			} else {
				//Modo con ID seguro
				error_log("Entrando a modo con ID seguro...");
				$peticion = esclarece($_POST['t']);
				if ($peticion != "") {
					$prtParams = explode("|-|", $peticion);
				}
				
			}
			if ($prtParams != NULL && count($prtParams) == 3) {
				/***** Reglas de la petición:
				 * Primero, el nombre del archivo a imprmir...
				 * Segundo, el tamaño a imprimir, en string...
				 * Tercero, el ID para imprimir...
				*/
				$url = DOMINIO . str_replace('tlacuilo.php', 'formatos/', $_SERVER['REQUEST_URI']).$prtParams[0];
				$nomArch = tempnam("/var/tmp", "tlacuilo");
				if ($nomArch) {
					//chromium --headless --disable-gpu --print-to-pdf=chromtest.pdf https://lab.achichincle.net
					shell_exec("chromium --headless --disable-gpu --user-agent=Tlacuilo_v0.9 --print-to-pdf=$nomArch.pdf $url?a={$prtParams[2]}");
					header('Content-Type: application/pdf');
					header("Content-Disposition:attachment;filename='informe_$prtParams[2].pdf'");
					readfile("$nomArch.pdf");
					unlink("$nomArch.pdf");
				}
			} else {
				registraError("TLACUILO CON POSIBLE AGRESIÓN", 90);
				error_log("consulta: " . print_r($_POST, true));
			}
		} else {
			//Error validando el query
			registraError("TLACUILO CONSULTA INVÁLIDA", 90);
			error_log("consulta: " . print_r($_POST, true));
		}
	} else {
		//No hay usuario
		registraError("TLACUILO SESIÓN INVÁLIDA", 90);
	}
} else {
	//Le falta barrio
	error_log("Origen detectado: " . $_SERVER["HTTP_ORIGIN"]);
	registraError("TLACUILO FUERA DE DOMINIO", 100);
}
function registraError($tipo, $severidad) {
	//Volcamos el error al archivo en disco, o lo enviamos al manejador de errores
	//TODO Aquí va la línea del manejador de errores y firewall
	//A mayor severidad, más problemas
	error_log("Hanumat haciendo su trabajo: $tipo, $severidad");
}
?>