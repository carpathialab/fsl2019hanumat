<?php
	/*	Se recomienda tener un usuario en la DB por cada aplicación.
	*	El usuario así mismo debe de tener solamente los privilegios
	*	SELECT, INSERT, UPDATE, EXECUTE
	*	y el privilegio DELETE se establece SOLAMENTE para las tablas que vayan a poder eliminar registros.
*/
	function conectaDB() {
		$servidor = 'localhost';	//Servidor de base de datos, no está obligado que sea local
		$dbnom = 'pvhanumat';	//Nombre de la base de datos
		$usuario = 'usrpvhanumat';	//Usuario para la aplicación de la base de datos
		$password = 'fsl2019';	//Contraseña
		$puerto = 3306;	//Puerto para el servidor...
		$conectID=new mysqli($servidor, $usuario, $password, $dbnom, $puerto);
		if(!$conectID->connect_error) {
			$conectID->set_charset('utf8');
			return $conectID;
		} else {
			return null;
		}
	}
?>
