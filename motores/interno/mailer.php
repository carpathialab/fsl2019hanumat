<?php
/**********************************************************************************************
*    Hanumat. PHP framework for fast and secure web application development
*
*    This file is part of Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*
*    Hanumat is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
************************ Español ********************************
*
*    Hanumat. Entorno de trabajo PHP para desarrollo rápido y seguro de aplicaciones
*
*    Este archivo es parte de Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat es software libre: usted puede redistribuirlo y/o modificarlo
*    bajo los términos de la Licencia Pública General GNU Affero tal y como
*    ha sido publicada por la Free Software Foundation, tanto la versión 3
*    de la Licencia o cualquier otra posterior.
*
*    Hanumat es distribuido en la esperanza de que llegue a ser útil,
*    pero SIN NINGUNA GARANTÍA; incluso sin implicar la garantía de
*    MERCHANTABILITY o FITNESS FOR A PARTICULAR PURPOSE.  Por favor refiérase a la
*    Licencia Pública General GNU Affero para más detalles.
*
*    Usted tiene que haber recibido una copia de la Licencia Pública General GNU Affero
*    con éste programa. De no ser así, vea <https://www.gnu.org/licenses/>.
*
**************************************************************************************/
require_once('defs.php');
set_include_path('/usr/share/php');
require_once('Mail.php');
require_once('Mail/mime.php');
function enviamsg($mensaje, $destino, $asunto, $adjunto = "") {
	$headers = array ('From' => CORREO_ORIGEN, 'To' => $destino, 'Subject' => $asunto, 'Content-Type' => 'text/html; charset=UTF-8');
	$mime = new Mail_mime(array('eol' => "\r\n", 'text_encoding' => 'utf8', 'html_encoding' => 'utf-8', 'text_charset' => 'utf-8', 'head_charset'=>'utf-8', 'html_charset' => 'utf-8'));
	$mime->setTXTBody("Para ver mejor este mensaje, active el HTML. $mensaje");
	$mime->setHTMLBody($mensaje);
	if ($adjunto != "") {
		if (!($err = $mime->addAttachment($adjunto, mime_content_type($adjunto)))) {
			error_log("No pudo adjuntar el archivo $adjunto");
			error_log(print_r($err, true));
		}
	}
	$cuerpo = $mime->get();
	$enc = $mime->headers($headers);
	$smtp = Mail::factory('smtp', array ('host' => CORREO_SERVIDOR, 'port' => CORREO_PUERTO, 'auth' => true, 'username' => CORREO_USUARIO, 'password' => CORREO_PASSWD));
	$correo = $smtp->send($destino, $enc, $cuerpo);
	if (PEAR::isError($correo)) {
		error_log($correo->getMessage());
		return false;
	} else {
		return true;
	}
}
?>