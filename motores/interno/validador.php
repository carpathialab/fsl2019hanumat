<?php
/**********************************************************************************************
*    Hanumat. PHP framework for fast and secure web application development
*
*    This file is part of Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*
*    Hanumat is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
************************ Español ********************************
*
*    Hanumat. Entorno de trabajo PHP para desarrollo rápido y seguro de aplicaciones
*
*    Este archivo es parte de Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat es software libre: usted puede redistribuirlo y/o modificarlo
*    bajo los términos de la Licencia Pública General GNU Affero tal y como
*    ha sido publicada por la Free Software Foundation, tanto la versión 3
*    de la Licencia o cualquier otra posterior.
*
*    Hanumat es distribuido en la esperanza de que llegue a ser útil,
*    pero SIN NINGUNA GARANTÍA; incluso sin implicar la garantía de
*    MERCHANTABILITY o FITNESS FOR A PARTICULAR PURPOSE.  Por favor refiérase a la
*    Licencia Pública General GNU Affero para más detalles.
*
*    Usted tiene que haber recibido una copia de la Licencia Pública General GNU Affero
*    con éste programa. De no ser así, vea <https://www.gnu.org/licenses/>.
*
**************************************************************************************/
require_once("motores/interno/defs.php");
$hayUsuario = (isset($_SESSION['Usuario']) && (isset($_SESSION['Usuario']['publica']) && $_SESSION['Usuario']['publica'] != "invalida"));
function redir($pagina) {
	error_log("Pide la redireccion a $pagina");
	header('Status: 301 Moved Permanently', false, 301);
	header("Location: $pagina");
	die();
	//Muelle, canalla
}

if (!$hayUsuario) {
	session_destroy();
	error_log(print_r($_SESSION['Usuario']));
	redir(PAG_DEFAULT);
	//var_dump($_SESSION);
} else {
	//FIXIT Por lo pronto todos tienen permisos
	if (!isset($_SESSION['Usuario']['permisos'][basename($_SERVER['PHP_SELF'], ".php")]) && FALSE) {
		//error_log("Entrando a página: " . basename($_SERVER['PHP_SELF'], ".php"));
		redir($_SESSION['Usuario']['pag_inicial']);
		//echo("permisos");
	}
}
?>