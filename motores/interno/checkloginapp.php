<?php
/**********************************************************************************************
*    Hanumat. PHP framework for fast and secure web application development
*
*    This file is part of Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*
*    Hanumat is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
************************ Español ********************************
*
*    Hanumat. Entorno de trabajo PHP para desarrollo rápido y seguro de aplicaciones
*
*    Este archivo es parte de Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat es software libre: usted puede redistribuirlo y/o modificarlo
*    bajo los términos de la Licencia Pública General GNU Affero tal y como
*    ha sido publicada por la Free Software Foundation, tanto la versión 3
*    de la Licencia o cualquier otra posterior.
*
*    Hanumat es distribuido en la esperanza de que llegue a ser útil,
*    pero SIN NINGUNA GARANTÍA; incluso sin implicar la garantía de
*    MERCHANTABILITY o FITNESS FOR A PARTICULAR PURPOSE.  Por favor refiérase a la
*    Licencia Pública General GNU Affero para más detalles.
*
*    Usted tiene que haber recibido una copia de la Licencia Pública General GNU Affero
*    con éste programa. De no ser así, vea <https://www.gnu.org/licenses/>.
*
**************************************************************************************/
//session_start();
require_once("./defs.php");
require_once("./funciones.php");
require_once("./conexion.php");
if (isset($_POST['token']) && esclarece($_POST['token']) == "hanumappelpoderoso!") {
	$dbcon = conectaDB();
	$retval = array("error" => "1", "errmsg"=>"Error masivo");
	$esRefirmado = FALSE;
	//Tal como está configurado, saca la sesión de otro lugar una vez que ingrese.
	error_log("Datos de firmado: ".print_r($_POST, true));
	//TODO quizás deberíamos agregar aquí alguna validación de un celular bloqueado...
	if (isset($_POST['usr'])) {
		$consulta = $dbcon->prepare("SELECT idusuario, rol, nombrecompleto, permisos, generaTokenSesion(idusuario, '".$_POST['q']."') as token FROM virt_usuario WHERE email = ? and passwd=password(?);");
		$consulta->bind_param("ss", $_POST['usr'], $_POST['pwd']);
	} else if (isset($_POST['tt'])) {
		$consulta = $dbcon->prepare("SELECT idusuario, rol, nombrecompleto, permisos, '1' as token FROM virt_usuario WHERE idusuario = ? and token = ?;");
		$consulta->bind_param("ss", $_POST['idusr'], $_POST['tt']);
		$esRefirmado = true;
	}
	$consulta->execute();
	//$consulta->store_result();
	$consulta->bind_result($id, $rol, $nombre, $permisos, $pag);
	$privada = "";
	$retval["error"] = "1";
	$retval["errmsg"] = "Error credenciales";
	while ($consulta->fetch()) {
		//Generar la llave pública y privada, y guardar la sesión en la base de datos
		if (!$esRefirmado) {
			$configSSL = array('private_key_bits' => 512);
			$res = openssl_pkey_new($configSSL);
			openssl_pkey_export($res, $privada);
			$publica = openssl_pkey_get_details($res);
			$publica = $publica['key'];
			$retval["publica"] = $publica;
		}
		$retval["error"] = "0";
		$retval["token"] = $pag;
		$retval["id"] = $id;
		$retval["rol"] = $rol;
		$retval["nombre"] = $nombre;
		$retval["perms"] = $permisos;
	}
	$consulta->close();
	if ($retval["error"] == "0" && !$esRefirmado) {
		$qry = "update virt_usuario set privada = '{$privada}', ultimo_login = now(), token = '{$retval['token']}' where idusuario = '{$id}';";
		if ($dbcon->query($qry)) error_log("Dice que pudo poner la privada");
		else error_log("Dice que: " . $dbcon->error);
	}
	$dbcon->close();
	echo json_encode($retval);
} else {
	header('HTTP/1.0 404 Not Found', true, 404);
	//var_dump($_POST);
	die();
}
?>
